<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="style.css">
<style>


</style>

<meta charset="UTF-8">
</head>
<body>

<?php
	include_once 'includes/psl-config.php';
	include_once 'includes/db_connect.php';
	include_once 'includes/functions.php';
	require_once 'includes/Table.php';
	
	session_start();

	$id = $_SESSION['user_id'];
	
	if (login_check($mysqli) == false) header('Location: login.php');
	
	echo ' <div class="alert alert-dismissable alert-success">	
		   <h1> Protocolos registrados </h1>
		   </div>';
			
	

	$attributes = array('class'=>"table table-striped table-hover", 'style'=>"width:70%");
	$table = new HTML_Table($attributes);
	
	$table->setHeaderContents(0,0,"Nombre de Proyecto");
	$table->setHeaderContents(0,1,"Estado");
	$table->setHeaderContents(0,2,"Fecha Registro");
	$table->setColAttributes( 0 , 'width="70%" ' , null );
	$table->setColAttributes( 1 , 'width="15%" align="center"' , null );
	$table->setColAttributes( 2 , 'width="15%"' , null );
	
	$query = "SELECT titulo, estado, fecha FROM login.protocolos WHERE user_id = " . $id . " ORDER BY fecha DESC";
	
	$stmt = $mysqli->prepare($query);
	$stmt->execute();
	$stmt->bind_result($titulo, $estado, $fecha);
	
	$rownum=1;
	
	while ($stmt->fetch()) {
		$table->setCellContents($rownum,0,$titulo);
		$table->setCellContents($rownum,1,$estado);
		$table->setCellContents($rownum,2,$fecha);
		//$table->setRowAttributes( $rownum , 'class="info"' , null );
		$rownum++;		
	}
	//$table->altRowAttributes(1, null, array("class"=>"info"));
	echo '<div class="container">';
	echo $table->toHTML();
	$mysqli->close();

	
?>

<p> <a href='panel.php'>Regresar al Menu</a><p>
<p> <a href='logout.php'> Salir </a></p>
</div>
</body>
</html>
	