<?php
	include_once 'includes/db_connect.php';	
	include_once 'includes/functions.php';
	include_once 'includes/Table.php';
	session_start();
	if (login_check($mysqli) == false) header('Location: login.php');	
?>
<html>
<head>
<link rel="stylesheet" type="text/css" href="style.css">
<title>Fecha Límite</title>
</head>
<body>

<div class="alert alert-success">
<h1>Establecer Fecha Límite de registro.</h1>
</div>

<?php 
	
	$query = "SELECT fecha_limite FROM periodos WHERE idperiodos= 1";
	$stmt = $mysqli->prepare($query);
	$stmt->execute();
	$stmt->bind_result($actual);
	$sttr = $stmt->fetch();

?>
<div class="container">
	<form action="periodo_proc.php" method="post" class="form-horizontal">
	<fieldset>
		<div class="form-group">
			<label for="flimite" class="col-lg-2 control-label">Fécha Límite: </label> 
			<div class="col-lg-10">
				<input class="form-control" type="date" name="flimite" id="flimite" value="<?php echo $actual;?>"></input>
			</div>
		</div>
		<div class="form-group">
			<div class="col-lg-10 col-lg-offset-2">
				<button type="submit" class="btn btn-primary">Cambiar</button>
			</div>
		</div>
	</fieldset>
	</form>
<p><a href="panel.php">Regresar al Menú.</a></p>
<p><a href="logout.php">Salir</a></p>
</div>
</body>
</html>