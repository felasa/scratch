<?php
	//LISTAR PROTOCOLOS PENDIENTES DE APROBACION
	//Poder cambiar estado

	include_once 'includes/db_connect.php';	
	include_once 'includes/functions.php';
	include_once 'includes/Table.php';
	session_start();
	if (login_check($mysqli) == false) header('Location: login.php');

?>

<html>
<head>
<meta charset="UTF-8"></meta>
<link rel="stylesheet" type="text/css" href="style.css">
<title>Estado</title>
</head>
<body>
<div class="container">
<?php 	
	$query = "SELECT protocolos_id, apaterno, nombre, titulo, estado, fecha 
				       FROM members 
				       JOIN protocolos 
				       ON members.id = user_id
				       WHERE estado = 'Pendiente' 
					   AND fecha < (SELECT fecha_limite FROM periodos WHERE idperiodos= 1)
					   ORDER BY fecha ASC";

	$stmt = $mysqli->prepare($query);
	$stmt->execute();
	$stmt->store_result();
	$nrows = $stmt->num_rows;
	$stmt->bind_result($uid, $ap, $nom, $tit, $est, $tmstmp);
	$nrows = $stmt->num_rows;
	$attributes = array('class'=>"table table-striped table-hover", 'style'=>'width:70%');
	$table = new HTML_Table($attributes);

	$rownum=1;
	$table->setHeaderContents(0,0,"Apellido");
	$table->setHeaderContents(0,1,"Nombre");
	$table->setHeaderContents(0,2,"Titulo");
	$table->setHeaderContents(0,3,"Estado");
	$table->setHeaderContents(0,4,"Fecha de Registro");
	$table->setColAttributes( 0 , 'width="11%"' , null );
	$table->setColAttributes( 1 , 'width="11%"' , null );
	$table->setColAttributes( 2 , 'width="51%"' , null );
	$table->setColAttributes( 3 , 'width="11%"' , null );
	$table->setColAttributes( 4 , 'width="16%"' , null );
	
	$rownum=1;
?>
<h1> Estado de proyectos</h1>
<form action="actualiza.php" method="post">

<?php
	echo "Lista de ". $nrows. " proyectos pendientes de aprobación registrados antes de la fecha límite.";
	$IDS = array();
	$ESTADOS = array();
	while ($stmt->fetch()) {
		$IDS[$rownum-1] = $uid;
		$ESTADOS[$rownum-1]=$est;
		$table->setCellContents($rownum,0,$ap);
		$table->setCellContents($rownum,1,$nom );
		$table->setCellContents($rownum,2,$tit);
		$table->setCellContents($rownum,3,
		'<select class="form-control" name="estados[]" selected="'. $est . '"><option value="Pendiente">Pendiente</option>
		<option value="Aceptado">Aceptado</option><option value="Rechazado">Rechazado</option></select>'
												);
		$table->setCellContents($rownum,4,$tmstmp);
		$rownum++;		
		}
	echo $table->toHTML(); 	
?>
<input type="hidden" value="<?php echo serialize($IDS);?>" name="ids"></input>
<button type="submit" class="btn btn-primary"> Actualizar</button>

</form>
</div>
<div class="container">
<p><a href="panel.php">Regresar al Menú.</a></p>
<p><a href="logout.php">Salir</a></p>
</div>
</body>
</html>
