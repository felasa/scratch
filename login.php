<?php
	include_once 'includes/db_connect.php';
	include_once 'includes/functions.php';
	session_start();
 
	if (login_check($mysqli) == true) {
		header('Location: panel.php');
	}
    
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Secure Login: Log In</title>
        <link rel="stylesheet" type="text/css" href="style.css">
        <script type="text/JavaScript" src="js/sha512.js"></script> 
        <script type="text/JavaScript" src="js/forms.js"></script> 
    </head>
    <body>
	<div class="container">
		<form  action="process_login.php" method="post" name="login_form">
			<fieldset>
			<legend> Ingreso </legend>
			<?php
			if (isset($_GET['error'])) {
				echo '<p class="error">Error</p>';
			}
			?> 
			
			<div class="form-group">                              
				<label for="email" class="col-lg-2 control-label">Email:</label> 
				<div class="col-lg-10">
					<br><input class="form-control" type="text" name="email" id="email"/></r>
				</div>
			</div>
			
			<div class="form-group">
				<label for="password" class="col-lg-2 control-label">Password:</label> 
				<div class="col-lg-10">
					<input class="form-control" type="password" name="password" id="password"/>
				</div>
			</div>
			<div class="form-group">
			<div class="col-lg-10 col-lg-offset-2">
				<button type="submit" class="btn btn-primary"                    
					   onclick="formhash(this.form, this.form.password);" > Ingresa </button>
			</div>
			</div>
			</fieldset>
			</form>
			
			<p>Si no tiene cuenta vaya a <a href="registro.php">registro</a></p>
		</div>
		
    </body>
</html>