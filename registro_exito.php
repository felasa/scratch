<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Registrado!</title>
        <link rel="stylesheet" type="text/css" href="style.css">
    </head>
    <body>
		<div class="container">
			<h1>El registro se realizó correctamente</h1>
			<p>Puede ahora <a href="login.php"> iniciar sesión</a> </p>
		</div>
    </body>
</html>