<?php
	include_once 'functions.php';
	include_once 'db_connect.php';
	session_start();
	if ( login_check($mysqli) == false ) {
		header('Location: login.php');
	} 
?>