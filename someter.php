<?php 
	include_once 'includes/db_connect.php';
	include_once 'includes/functions.php';
	session_start();
	if (login_check($mysqli) == false) header('Location: login.php');
	$query = "SELECT fecha_limite FROM periodos WHERE idperiodos= 1";
	$stmt = $mysqli->prepare($query);
	$stmt->execute();
	$stmt->bind_result($plazo);
	$sttr = $stmt->fetch(); 
?>
	
<html>
<head>
<link rel="stylesheet" type="text/css" href="style.css">
<title>Registro de protocolo</title>
</head>
<body>

<?php
	$hoy = date('Y-m-d');	
	$hoy = strtotime($hoy);
	$plazo = strtotime($plazo);
	if ($hoy - $plazo > 0 ) {
		echo '<div class="alert alert-dismissable alert-danger">
				<h3>Aviso:</h3>
				<p>La fecha límite para revisión en este periodo ha expirado. Puede continuar con el registro para ser evaluado en el próximo periodo.</p>
			  </div>';
	}
	echo '<br>';
	echo '<div class="container">
			<form class="form-horizontal" action="upload_script.php" method="post" enctype="multipart/form-data">
				<fieldset>	
					<p style=" padding-left: 3%; padding-right: 0px; position: relative; ">
						<legend>Registro de protocolo </legend>
						<label for="file"  >Archivo:</label>		
						<input type="file" id="file" name="file" ></input>
					</p>		
					<div class="form-group">
						<label for="titulo" class="col-lg-2 control-label">Título del Proyecto:</label>
						<textarea type="text" class="form-control" name="titulo" id="titulo" rows="3" cols="50" ></textarea>
					</div>
					<div class="form-group">	
						<label for="submit" class="col-lg-2 control-label"></label>
						<button class="btn btn-primary" type="submit" name="submit">Registrar</button>
					</div>				
				</fieldset>
			</form>
			<p style="padding-left: 3%">
				<a href="panel.php">Regresar</a>
			</p>
		</div>'
?>

</body>
</html>