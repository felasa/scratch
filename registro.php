<?php 
	include_once "includes/registro_inc.php";
?>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Registro</title>
		<link rel="stylesheet" type="text/css" href="style.css">
		<script type="text/JavaScript" src="js/sha512.js"></script> 
        <script type="text/JavaScript" src="js/forms.js"></script>
	</head>
	<body>
		<div class="container">
			<form class="form-horizontal" action="<?php echo $_SERVER['PHP_SELF']; ?>"			
			method="post" name="formulario_registro">
				<fieldset>
					<legend>Registro</legend>
					<div class="form-group">
						<label for="apaterno" class="col-lg-2 control-label">Apellido Paterno:</label>
						<input type="text" name="apaterno" id="apaterno" class="form-control"/> 
					</div>
					<div class="form-group">
						<label for="amaterno" class="col-lg-2 control-label">Apellido Materno:</label>
						<input type="text" name="amaterno" id="amaterno" class="form-control"/>
					</div>
					<div class="form-group">
						<label for="nombre" class="col-lg-2 control-label">Nombre(s):</label>
						<input type="text" name="nombre" id="nombre" class="form-control"/>
					</div>
					<div class="form-group">
						<label for="email" class="col-lg-2 control-label">Email:</label>
						<input type="text" name="email" id="email" class="form-control"/>
					</div>
				<div class="form-group"> 
					<label for="pwd" class="col-lg-2 control-label">Contraseña:</label>
					<input type="password" name="pwd" id ="pwd" class="form-control" />
				</div>
				<div class="form-group">
					<label for="pwd2" class="col-lg-2 control-label">Confirmar contraseña:</label>
					<input type="password" name="pwd2" class="form-control"/>
				</div>
				<div class="form-group">
					<div class="col-lg-10 col-lg-offset-2">
						<button type="submit" value="Register" 
						onclick="return regformhash(this.form, this.form.apaterno, this.form.amaterno,
						this.form.nombre,
						this.form.email,
						this.form.pwd,
						this.form.pwd2);" class="btn btn-primary" > Registrar </button>
					</div>
				</div>
			</fieldset>
		</form>
		</div>
		<div class="container">
			<?php echo $error_msg ;?>
		</div>
	</body>
</html>