<?php
	include_once 'includes/db_connect.php';
	include_once 'includes/functions.php';
	session_start();
	$fname = $_SESSION['nombre'] . " " . $_SESSION['apaterno'] . " " . $_SESSION['amaterno'];
	$grupo = $_SESSION['grupo'];
	if (login_check($mysqli) == false) header('Location: login.php');
?>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
<div class="container">
<?php

	echo "
		<h1> Menú </h1>		
		<p> Inició session como  $fname. <a href='logout.php'> Cambiar </a> </p>
		<ul>
		<li><a href='someter.php'> Someter protocolo a evaluación. </a> </li>
		<li><a href='revisar.php'> Revisar estado de protocolos. </a> </li>
		</ul>";
	if ($grupo == 'admin') {
		echo " Admin:
		<ul>
		<li> <a href='listado.php'> Descargar protocolos. </a></li>
		<li> <a href='periodo.php'>Establecer periodos de registro. </a> </li>
		<li> <a href='estado.php'> Cambiar estado de protocolos. </a> </li>
		</ul>
		";
	}		 
?>
<p><a href="logout.php">Salir</a></p>
</div>
</body>
</html>