<?php
	include_once 'includes/db_connect.php';	
	include_once 'includes/functions.php';
	include_once 'includes/Table.php';
	session_start();
	if (login_check($mysqli) == false) header('Location: login.php');
?>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" type="text/css" href="style.css">

</head>
<body>
<div class="container">
<h1>Descargar protocolos registrados</h1>
</div>
 <?php 
 	$izq = $_POST['izq'];
	$der = $_POST['der'];

	if (isset($_POST['izq'],$_POST['der'])) {

		$stmt = " SELECT apaterno, nombre, titulo, estado, fecha 
				       FROM members 
				       JOIN protocolos 
				       ON members.id = user_id
				       WHERE fecha BETWEEN '".$izq ."' AND '".$der ."'"; 
					   
		$stmt = $mysqli->prepare($stmt);

		$stmt->execute();
		$stmt->bind_result($apellido, $nombre, $tit, $est, $tmtp);
		$attributes = array('class'=>"table table-striped table-hover");
		$table = new HTML_Table($attributes);

		$table->setHeaderContents(0,0,"Apellido");
		$table->setHeaderContents(0,1,"Nombre");
		$table->setHeaderContents(0,2,"Titulo");
		$table->setHeaderContents(0,3,"Estado");
		$table->setHeaderContents(0,4,"Fecha de Registro");
		$table->setColAttributes( 0 , 'width="11%"' , null );
		$table->setColAttributes( 1 , 'width="11%"' , null );
		$table->setColAttributes( 2 , 'width="56%"' , null );
		$table->setColAttributes( 3 , 'width="11%"' , null );
		$table->setColAttributes( 4 , 'width="11%"' , null );
		$rownum=1;
		while ($stmt->fetch()) {
				$table->setCellContents($rownum,0,$apellido);
				$table->setCellContents($rownum,1,$nombre);
				$table->setCellContents($rownum,2,$tit);
				$table->setCellContents($rownum,3,$est);
				$table->setCellContents($rownum,4,$tmtp);
				$rownum++;		
		}
		echo '<div class="container">';
		echo $table->toHTML();
		echo '	<form action="descarga.php" method="post">
					<fieldset>
						<input type="hidden" name="izq" value="'.$_POST['izq'].'" ></input>
						<input type="hidden" name="der" value="'.$_POST['der'].'" ></input>
						<div class="form-group">
							<div class="col-lg-10 col-lg-offset-2">
								<button type="submit" name="dload" value="Descarga" class="btn btn-primary">Descarga</button>
							</div>
						</div>
					</fieldset>
				</form>';
		echo '</div>';
		
	}
?>

<div class="container">
<form class="form-horizontal" action="<?php esc_url($_SERVER['PHP_SELF']);?>" method="post" name="descarga" id="descarga" >
	<fieldset>
	<legend>Listar protocolos registrados</legend>
	<div class="form-group">
		<label for="izq" class="col-lg-2 control-label">Entre</label> 
		<div class="col-lg-10">
			<input type="date" name="izq" label="izq"> </input>
		</div>
	</div>
	<div class="form-group">
		<label for="der" class="col-lg-2 control-label">y</label> 
		<div class="col-lg-10">
			<input type="date" name="der" label="der"></input>
		</div>
	</div>
	<div class="form-group">
		<div class="col-lg-10 col-lg-offset-2">
			<button type="submit" class="btn btn-primary">Enviar</button>
		</div>
	</div>
</form>	
</div>



<div class="container">
<p><a href="panel.php">Regresar al Menú.</a></p>
<p><a href="logout.php">Salir</a></p>
</div>
</body>
</html>